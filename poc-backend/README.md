# Inventory Backend POC 

## Requirements

- Python 3.11
- Poetry
- Docker

## Installation

```shell
$ pip install poetry==1.6.1
$ poetry install
```

## Run

```shell
$ make db
$ make up
```
