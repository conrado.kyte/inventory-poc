from fastapi import FastAPI

import app.config as config
from app.logger import logger
from app.router.routes import api_router

logger.info(f"Config loaded for env: {config.ENV}")

api = FastAPI()


@api.exception_handler(Exception)
async def global_exception_handler(request, exc: Exception):
    logger.error(f"Unhandled error {exc}", exc_info=True)
    return {"detail": "Internal Server Error"}, 500


api.include_router(api_router)
