from datetime import datetime, timezone


def create_timestamp() -> datetime:
    return datetime.now(tz=timezone.utc)
