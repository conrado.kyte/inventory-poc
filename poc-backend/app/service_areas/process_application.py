from uuid import NAMESPACE_URL, uuid5

from eventsourcing.application import AggregateNotFound
from eventsourcing.dispatch import singledispatchmethod
from eventsourcing.domain import Aggregate, event
from eventsourcing.system import ProcessApplication

from app.service_areas.aggregate import ServiceAreas


class Counter(Aggregate):
    def __init__(self, name):
        self.name = name
        self.count = 0

    @classmethod
    def create_id(cls, name):
        return uuid5(NAMESPACE_URL, f"/counters/{name}")

    @event("Incremented")
    def increment(self):
        self.count += 1


class Counters(ProcessApplication):
    @singledispatchmethod
    def policy(self, domain_event, process_event):
        """Default policy"""

    @policy.register(ServiceAreas.ServiceAreaAdded)
    def _(self, domain_event, process_event):
        service_area = domain_event.service_area
        try:
            counter_id = Counter.create_id(service_area)
            counter = self.repository.get(counter_id)
        except AggregateNotFound:
            counter = Counter(service_area)
        counter.increment()
        process_event.collect_events(counter)

    def get_count(self, service_area):
        counter_id = Counter.create_id(service_area)
        try:
            counter = self.repository.get(counter_id)
        except AggregateNotFound:
            return 0
        return counter.count
