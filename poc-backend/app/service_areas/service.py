import json
from datetime import datetime

from eventsourcing.application import AggregateNotFound
from eventsourcing.system import SingleThreadedRunner, System

from app.logger import logger
from app.service_areas.aggregate import ServiceAreas
from app.service_areas.application import ServiceAreasApplication
from app.service_areas.process_application import Counters

service_area_app = ServiceAreasApplication()


class ServiceAreasService:
    def __init__(self):
        logger.info("Initializing ServiceAreasService...")
        system = System(pipes=[[ServiceAreasApplication, Counters]])
        runner = SingleThreadedRunner(system)
        runner.start()

        self._service_areas_app = runner.get(ServiceAreasApplication)
        self._counters = runner.get(Counters)

    def get_service_areas(self) -> ServiceAreas:
        logger.info("Fetching service areas...")
        try:
            return self._service_areas_app.get_service_areas()
        except AggregateNotFound:
            logger.info("Service areas not found, creating...")
            self._service_areas_app.create_service_areas()
            return self._service_areas_app.get_service_areas()
        except Exception as e:
            logger.info(f"Error getting ServiceAreas: {e}")
            raise e

    def add_service_area(self, service_area: str) -> ServiceAreas:
        logger.info(f"Adding service area: {service_area}")
        service_areas = self.get_service_areas()

        if service_area in service_areas.service_areas:
            return service_areas

        return self._service_areas_app.add_service_area(service_area)

    def get_service_areas_events(
        self,
        page: int,
    ):
        logger.info("Fetching service areas events...")
        start = (page - 1) * 10
        events = self._service_areas_app.notification_log.select(
            start=start, limit=10, stop=None, topics=()
        )
        return [
            {"topic": event.topic, "state": json.loads(event.state.decode("utf-8"))}
            for event in events
        ]

    def snapshot_service_areas(self):
        logger.info("Taking snapshot of service areas...")
        service_areas = self.get_service_areas()
        return self._service_areas_app.take_snapshot(service_areas.id)

    def get_snapshot_service_areas(self):
        logger.info("Fetching service areas snapshot...")
        service_areas = self.get_service_areas()
        return (
            self._service_areas_app.snapshots.get(service_areas.id, desc=True, limit=1)
            if self._service_areas_app.snapshots
            else None
        )

    def get_service_areas_at_timestamp(self, timestamp: datetime):
        logger.info(f"Fetching service areas at timestamp: {timestamp}")
        service_areas = self.get_service_areas()
        return self._service_areas_app.get_service_areas_at_timestamp(
            service_areas.id, timestamp
        )

    def get_count(self, service_area):
        return self._counters.get_count(service_area)
