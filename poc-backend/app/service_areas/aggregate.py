from typing import List
from uuid import NAMESPACE_URL, uuid5

from eventsourcing.domain import Aggregate, AggregateCreated, event


class Created(AggregateCreated):
    service_area: str


class ServiceAreas(Aggregate):
    """ServiceAreas Aggregate

    Args:
        Aggregate (ServiceAreas): ServiceAreas Aggregate
    """

    @event("ServiceAreasCreated")
    def __init__(
        self,
        service_areas: List[str],
    ):
        self.service_areas = service_areas

    @event("ServiceAreaAdded")
    def add_service_area(self, service_area: str) -> None:
        self.service_areas.append(service_area)

    @staticmethod
    def create_id():
        return uuid5(NAMESPACE_URL, "#service_areas")
