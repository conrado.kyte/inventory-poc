import base64
from datetime import datetime

from fastapi import HTTPException, Query
from pydantic import BaseModel

from app.router.api_router import api_router as service_areas_router
from app.service_areas.service import ServiceAreasService

service_area_service = ServiceAreasService()


class AddServiceArea(BaseModel):
    name: str


@service_areas_router.get("/service_areas")
def get_service_areas(timestamp: str | None = None):
    if not timestamp:
        return service_area_service.get_service_areas()

    try:
        decoded_timestamp = base64.b64decode(timestamp).decode("utf-8")  # type: ignore
        datetime_object = datetime.fromisoformat(decoded_timestamp)
        return service_area_service.get_service_areas_at_timestamp(datetime_object)
    except ValueError:
        raise HTTPException(
            status_code=422,
            detail="Input should be a valid datetime, invalid timezone sign",
        )


@service_areas_router.post("/service_areas")
def add_service_areas(add_service_area: AddServiceArea):
    return service_area_service.add_service_area(add_service_area.name)


@service_areas_router.get("/service_areas/events")
def get_service_areas_events(page: int = Query(default=1, description="Events page")):
    return service_area_service.get_service_areas_events(page=page)


@service_areas_router.get("/service_areas/count")
def get_service_areas_count(service_area: str):
    return service_area_service.get_count(service_area)
