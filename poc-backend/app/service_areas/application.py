from datetime import datetime
from typing import cast
from uuid import UUID

from app.event_sourcing.persistence import SearchableTimestampsRecorder
from app.event_sourcing.searchable_timestamp_application import (
    SearchableTimestamApplication,
)
from app.logger import logger
from app.service_areas.aggregate import ServiceAreas


class ServiceAreasApplication(SearchableTimestamApplication):
    is_snapshotting_enabled = True

    def get_service_areas_at_timestamp(
        self, tracking_id: UUID, timestamp: datetime
    ) -> ServiceAreas:
        logger.info("Fetching service areas at timestamp with custom recorder...")
        recorder = cast(SearchableTimestampsRecorder, self.recorder)
        version = recorder.get_version_at_timestamp(tracking_id, timestamp)
        return cast(ServiceAreas, self.repository.get(tracking_id, version=version))

    def create_service_areas(self) -> ServiceAreas:
        service_areas = ServiceAreas([])
        self.save(service_areas)
        return service_areas

    def get_service_areas(self) -> ServiceAreas:
        uuid = ServiceAreas.create_id()
        return cast(ServiceAreas, self.repository.get(uuid))

    def add_service_area(self, service_area: str) -> ServiceAreas:
        service_areas = self.get_service_areas()
        service_areas.add_service_area(service_area.upper())
        self.save(service_areas)
        return service_areas
