import base64
from datetime import datetime

from fastapi import HTTPException, Query
from pydantic import BaseModel

from app.availability.service import AvailabilityService
from app.logger import logger
from app.router.api_router import api_router as availiability_router

availability_service = AvailabilityService()


class IncrementAvailability(BaseModel):
    start_date: str
    end_date: str | None = None


@availiability_router.get("/availability/events")
def get_availability_events(page: int = Query(default=1, description="Events page")):
    return availability_service.get_availability_events(page=page)


@availiability_router.get("/availability/{service_area}")
def get_availability(service_area: str, timestamp: str | None = None):
    if not timestamp:
        return availability_service.get_availability(service_area=service_area)

    try:
        decoded_timestamp = base64.b64decode(timestamp).decode("utf-8")  # type: ignore
        datetime_object = datetime.fromisoformat(decoded_timestamp)
        return availability_service.get_availability_at_timestamp(
            service_area=service_area, timestamp=datetime_object
        )
    except ValueError:
        raise HTTPException(
            status_code=422,
            detail="Input should be a valid datetime, invalid timezone sign",
        )


@availiability_router.post("/availability/{service_area}")
def add_availability(service_area: str, increment: IncrementAvailability):
    try:
        decoded_start = base64.b64decode(increment.start_date).decode("utf-8")  # type: ignore
        start_timestamp = datetime.fromisoformat(decoded_start)
        end_timestamp = None

        if increment.end_date:
            decoded_end = base64.b64decode(increment.end_date).decode("utf-8")  # type: ignore
            end_timestamp = datetime.fromisoformat(decoded_end)

        return availability_service.incremet_availability(
            service_area=service_area,
            from_timestamp=start_timestamp,
            to_timestamt=end_timestamp,
        )
    except ValueError:
        raise HTTPException(
            status_code=422,
            detail="Input should be a valid datetime, invalid timezone sign",
        )
