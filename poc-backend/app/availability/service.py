import json
from datetime import datetime
from typing import Sequence

from eventsourcing.application import AggregateNotFound

from app.availability.aggregate import Availability
from app.availability.application import AvailabilityApplication
from app.logger import logger


class AvailabilityService:
    def __init__(self):
        logger.info("Initializing AvailabilityService...")
        self._availability_app = AvailabilityApplication()

    def get_availability(self, service_area: str) -> Availability:
        logger.info("Fetching availability...")
        try:
            return self._availability_app.get_availability(service_area=service_area)
        except AggregateNotFound:
            logger.info("Availability not found, creating...")
            self._availability_app.create_availability(service_area=service_area)
            return self._availability_app.get_availability(service_area=service_area)
        except Exception as e:
            logger.info(f"Error getting Availability: {e}")
            raise e

    def incremet_availability(
        self,
        service_area: str,
        from_timestamp: datetime,
        to_timestamt: datetime | None = None,
    ) -> Availability:
        logger.info(f"Incrementing availability area: {service_area}")
        return self._availability_app.increment_availability(
            service_area=service_area,
            from_timestamp=from_timestamp,
            to_timestamp=to_timestamt,
        )

    def get_availability_events(
        self,
        page: int,
    ):
        logger.info("Fetching availability events...")
        start = (page - 1) * 10
        events = self._availability_app.notification_log.select(
            start=start, limit=10, stop=None, topics=()
        )
        return [
            {"topic": event.topic, "state": json.loads(event.state.decode("utf-8"))}
            for event in events
        ]

    def get_availability_at_timestamp(self, service_area: str, timestamp: datetime):
        logger.info(f"Fetching availability at timestamp: {timestamp}")
        availability = self.get_availability(service_area=service_area)
        return self._availability_app.get_at_timestamp(availability.id, timestamp)
