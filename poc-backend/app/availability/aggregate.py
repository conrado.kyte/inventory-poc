from collections import OrderedDict
from datetime import datetime
from uuid import NAMESPACE_URL, uuid5

from eventsourcing.domain import Aggregate, event

from app.availability.availability_updater import update_availability


class Availability(Aggregate):
    @event("Created")
    def __init__(self, service_area: str):
        self.service_area = service_area
        self.availability: OrderedDict = OrderedDict()

    @staticmethod
    def create_id(service_area: str):
        return uuid5(NAMESPACE_URL, f"/availability/{service_area.lower()}")

    @event("Incremented")
    def increment(self, from_timestamp: datetime, to_timestamp: datetime | None = None):
        self.availability = update_availability(
            start_datetime=from_timestamp,
            end_datetime=to_timestamp,
            current=self.availability,
        )
