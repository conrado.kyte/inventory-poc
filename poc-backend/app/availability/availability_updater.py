from collections import OrderedDict
from datetime import datetime
from typing import List


def update_availability(
    start_datetime: datetime,
    end_datetime: datetime | None = None,
    current: OrderedDict | None = None,
) -> OrderedDict:
    if current is None:
        current = OrderedDict()

    current = increment_start_availability(start_datetime, current)
    if end_datetime:
        current = increment_availability_in_range(start_datetime, end_datetime, current)
        current = decrement_end_availability(end_datetime, current)
    else:
        current = increment_availability_after_start(start_datetime, current)

    return current


def increment_start_availability(
    start_datetime: datetime, current: OrderedDict
) -> OrderedDict:
    if start_datetime.isoformat() in current:
        current[start_datetime.isoformat()]["availability"] += 1
    else:
        new_availability = get_new_availability(start_datetime, current, increment=1)
        current = insert_in_order(
            start_datetime.isoformat(), {"availability": new_availability}, current
        )
    return current


def increment_availability_in_range(
    start_datetime: datetime, end_datetime: datetime, current: OrderedDict
) -> OrderedDict:
    for key in current:
        if start_datetime < datetime.fromisoformat(key) < end_datetime:
            current[key]["availability"] += 1
    return current


def decrement_end_availability(
    end_datetime: datetime, current: OrderedDict
) -> OrderedDict:
    new_availability = get_new_availability(end_datetime, current, increment=-1)
    current = insert_in_order(
        end_datetime.isoformat(), {"availability": new_availability}, current
    )
    return current


def increment_availability_after_start(
    start_datetime: datetime, current: OrderedDict
) -> OrderedDict:
    start_reached = False
    for key in current:
        if start_reached:
            current[key]["availability"] += 1
        if key == start_datetime.isoformat():
            start_reached = True
    return current


def get_new_availability(
    datetime_obj: datetime, current: OrderedDict, increment: int
) -> int:
    previous_keys = get_previous_keys(datetime_obj, current)
    if previous_keys:
        closest_previous_key = previous_keys[-1]
        return current[closest_previous_key]["availability"] + increment
    return 1


def get_previous_keys(datetime_obj: datetime, current: OrderedDict) -> List[str]:
    return [key for key in current.keys() if datetime.fromisoformat(key) < datetime_obj]


def insert_in_order(key: str, value: dict, current: OrderedDict) -> OrderedDict:
    items = list(current.items())
    for i, (k, _) in enumerate(items):
        if datetime.fromisoformat(k) > datetime.fromisoformat(key):
            items.insert(i, (key, value))
            break
    else:
        items.append((key, value))
    return OrderedDict(items)
