from datetime import datetime
from typing import cast
from uuid import UUID

from app.availability.aggregate import Availability
from app.event_sourcing.persistence import SearchableTimestampsRecorder
from app.event_sourcing.searchable_timestamp_application import (
    SearchableTimestamApplication,
)
from app.logger import logger


class AvailabilityApplication(SearchableTimestamApplication):
    def get_at_timestamp(self, tracking_id: UUID, timestamp: datetime) -> Availability:
        logger.info("Fetching availability at timestamp with custom recorder...")
        recorder = cast(SearchableTimestampsRecorder, self.recorder)
        version = recorder.get_version_at_timestamp(tracking_id, timestamp)
        return cast(Availability, self.repository.get(tracking_id, version=version))

    def get_availability(self, service_area: str) -> Availability:
        uuid = Availability.create_id(service_area=service_area.lower())
        return cast(Availability, self.repository.get(uuid))

    def create_availability(self, service_area: str) -> Availability:
        availability = Availability(service_area=service_area.lower())
        self.save(availability)
        return availability

    def increment_availability(
        self,
        service_area: str,
        from_timestamp: datetime,
        to_timestamp: datetime | None = None,
    ) -> Availability:
        availability = self.get_availability(service_area=service_area.lower())
        availability.increment(from_timestamp=from_timestamp, to_timestamp=to_timestamp)
        self.save(availability)
        return availability
