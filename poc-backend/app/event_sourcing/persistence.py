from abc import abstractmethod
from datetime import datetime
from typing import Any, List, Optional, Sequence, Tuple, cast
from uuid import UUID

from eventsourcing.domain import Aggregate
from eventsourcing.persistence import ApplicationRecorder, StoredEvent
from eventsourcing.postgres import (
    Factory,
    PostgresApplicationRecorder,
    PostgresConnection,
    PostgresCursor,
    PostgresDatastore,
)

from app.logger import logger


class SearchableTimestampsRecorder(ApplicationRecorder):
    @abstractmethod
    def get_version_at_timestamp(
        self, originator_id: UUID, timestamp: datetime
    ) -> Optional[int]:
        """
        Returns originator version at timestamp for given originator ID.
        """


class SearchableTimestampsApplicationRecorder(
    SearchableTimestampsRecorder, PostgresApplicationRecorder
):
    def __init__(
        self,
        datastore: PostgresDatastore,
        events_table_name: str = "stored_events",
        event_timestamps_table_name: str = "event_timestamps",
    ):
        logger.info("Initializing SearchableTimestampsApplicationRecorder...")
        self.check_table_name_length(event_timestamps_table_name, datastore.schema)
        self.event_timestamps_table_name = event_timestamps_table_name
        super().__init__(datastore, events_table_name)
        self.insert_event_timestamp_statement = (
            f"INSERT INTO {self.event_timestamps_table_name} VALUES ($1, $2, $3)"
        )
        self.insert_event_timestamp_statement_name = (
            f"insert_{event_timestamps_table_name}".replace(".", "_")
        )
        self.select_event_timestamp_statement = (
            f"SELECT originator_version FROM {self.event_timestamps_table_name} WHERE "
            f"originator_id = $1 AND "
            f"timestamp <= $2 "
            "ORDER BY originator_version DESC "
            "LIMIT 1"
        )

        self.select_event_timestamp_statement_name = (
            f"select_{event_timestamps_table_name}".replace(".", "_")
        )

    def construct_create_table_statements(self) -> List[str]:
        logger.info("Constructing create table statements...")
        statements = super().construct_create_table_statements()
        statements.append(
            "CREATE TABLE IF NOT EXISTS "
            f"{self.event_timestamps_table_name} ("
            "originator_id uuid NOT NULL, "
            "timestamp timestamp with time zone, "
            "originator_version bigint NOT NULL, "
            "PRIMARY KEY "
            "(originator_id, timestamp))"
        )
        return statements

    def _prepare_insert_events(self, conn: PostgresConnection) -> None:
        logger.info("Preparing insert events...")
        super()._prepare_insert_events(conn)
        self._prepare(
            conn,
            self.insert_event_timestamp_statement_name,
            self.insert_event_timestamp_statement,
        )

    def _insert_events(
        self,
        c: PostgresCursor,
        stored_events: List[StoredEvent],
        **kwargs: Any,
    ) -> Optional[Sequence[int]]:
        logger.info("Entering _insert_events...")

        notification_ids = super()._insert_events(c, stored_events, **kwargs)

        logger.info("_insert_events 01")
        # Insert event timestamps.
        event_timestamps_data = cast(
            List[Tuple[UUID, datetime, int]], kwargs.get("event_timestamps_data")
        )
        logger.info(f"_insert_events 02 events length = {len(event_timestamps_data)}")
        for event_timestamp_data in event_timestamps_data:
            statement_alias = self.statement_name_aliases[
                self.insert_event_timestamp_statement_name
            ]
            logger.info(f"_insert_events statement_alias = {statement_alias}")
            c.execute(f"EXECUTE {statement_alias}(%s, %s, %s)", event_timestamp_data)
        return notification_ids

    def get_version_at_timestamp(
        self, originator_id: UUID, timestamp: datetime
    ) -> Optional[int]:
        with self.datastore.get_connection() as conn:
            self._prepare(
                conn,
                self.select_event_timestamp_statement_name,
                self.select_event_timestamp_statement,
            )
            with conn.transaction(commit=False) as curs:
                statement_alias = self.statement_name_aliases[
                    self.select_event_timestamp_statement_name
                ]
                logger.info(f"Fetching version at timestamp from {statement_alias} ...")
                curs.execute(
                    f"EXECUTE {statement_alias}(%s, %s)", [originator_id, timestamp]
                )
                for row in curs.fetchall():
                    return row["originator_version"]
                else:
                    return Aggregate.INITIAL_VERSION - 1


class SearchableTimestampsInfrastructureFactory(Factory):
    def application_recorder(self) -> ApplicationRecorder:
        logger.info("Initializing SearchableTimestampsInfrastructureFactory...")
        prefix = (self.datastore.schema + ".") if self.datastore.schema else ""
        prefix += self.env.name.lower() or "stored"
        events_table_name = prefix + "_events"
        event_timestamps_table_name = prefix + "_timestamps"
        logger.info(f"SearchableTimestampsInfrastructureFactory {events_table_name}")
        recorder = SearchableTimestampsApplicationRecorder(
            datastore=self.datastore,
            events_table_name=events_table_name,
            event_timestamps_table_name=event_timestamps_table_name,
        )
        recorder.create_table()
        return recorder


del Factory
