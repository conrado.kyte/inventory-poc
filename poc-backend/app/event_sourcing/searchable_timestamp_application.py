from typing import List

from eventsourcing.application import Application, ProcessingEvent
from eventsourcing.domain import Aggregate
from eventsourcing.persistence import Recording
from eventsourcing.utils import Environment

import app.config as config
from app.event_sourcing.persistence import SearchableTimestampsInfrastructureFactory
from app.logger import logger


class SearchableTimestamApplication(Application):
    _custom_recorder = None

    def construct_recorder(self):
        """Override the method to return the custom recorder."""
        if self._custom_recorder is None:
            logger.info("Initializing custom recorder...")
            env = {
                "POSTGRES_DBNAME": config.POSTGRES_DBNAME,
                "POSTGRES_HOST": config.POSTGRES_HOST,
                "POSTGRES_PORT": config.POSTGRES_PORT,
                "POSTGRES_USER": config.POSTGRES_USER,
                "POSTGRES_PASSWORD": config.POSTGRES_PASSWORD,
            }
            self._custom_recorder = SearchableTimestampsInfrastructureFactory(
                Environment(name=self.name, env=env)
            ).application_recorder()
        return self._custom_recorder

    def _record(self, processing_event: ProcessingEvent) -> List[Recording]:
        logger.info("Recording event with custom recorder...")
        event_timestamps_data = [
            (e.originator_id, e.timestamp, e.originator_version)
            for e in processing_event.events
            if isinstance(e, Aggregate.Event)
        ]
        processing_event.saved_kwargs["event_timestamps_data"] = event_timestamps_data
        return super()._record(processing_event)
