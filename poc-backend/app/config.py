import os

from dotenv import load_dotenv

load_dotenv()

ENV = os.getenv("ENV", "local")
PERSISTENCE_MODULE = os.getenv("PERSISTENCE_MODULE", "eventsourcing.postgres")
POSTGRES_DBNAME = os.getenv("POSTGRES_DB", "inventory")
POSTGRES_HOST = os.getenv("POSTGRES_HOST", "localhost")
POSTGRES_USER = os.getenv("POSTGRES_USER", "postgres")
POSTGRES_PASSWORD = os.getenv("POSTGRES_PASSWORD", "postgres")
POSTGRES_PORT = os.getenv("POSTGRES_PORT", "5432")
