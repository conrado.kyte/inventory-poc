import logging
import logging.config

from pythonjsonlogger import jsonlogger  # noqa
from rich.logging import RichHandler  # noqa

from app.config import ENV

LOGGING_CONFIG = {
    "version": 1,
    "formatters": {
        "json": {
            "()": "pythonjsonlogger.jsonlogger.JsonFormatter",
            "format": "%(asctime)s %(name)s %(levelname)s %(message)s",
        },
        "default": {
            "format": "%(asctime)s - %(name)s - %(levelname)s - %(message)s",
        },
    },
    "handlers": {
        "console": {
            "class": "logging.StreamHandler",
            "formatter": "json",
            "level": "DEBUG",
        },
        "file": {
            "class": "logging.FileHandler",
            "filename": "app.log",
            "formatter": "default",
            "level": "INFO",
        },
    },
    "loggers": {
        "": {
            "handlers": ["console", "file"],
            "level": "DEBUG",
        },
        "uvicorn.access": {
            "handlers": ["console", "file"],
            "level": "DEBUG",
        },
    },
}

if ENV == "local":
    LOGGING_CONFIG["handlers"]["console"] = {
        "class": "rich.logging.RichHandler",
        "formatter": "default",
        "level": "DEBUG",
    }


logging.config.dictConfig(LOGGING_CONFIG)
logger = logging.getLogger(__name__)
