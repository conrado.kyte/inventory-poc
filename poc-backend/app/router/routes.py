from app.availability.routes import availability_service  # noqa
from app.service_areas.routes import service_areas_router  # noqa

from .api_router import api_router


@api_router.get("/")
def read_root():
    return {"health": True}
