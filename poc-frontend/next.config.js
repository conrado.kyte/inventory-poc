/** @type {import('next').NextConfig} */
const nextConfig = {
  experimental: {
    serverActions: true,
  },
  async rewrites() {
    return [
      {
        source: "/api/service_areas/events",
        destination: "http://127.0.0.1:8000/service_areas/events",
      },
      {
        source: "/api/availability/events",
        destination: "http://127.0.0.1:8000/availability/events",
      },
    ];
  },
};

module.exports = nextConfig;
