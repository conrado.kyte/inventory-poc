import { AvailabilityEvent } from "@/types/availability";

export async function getAvailabilityEvents(page: number) {
  const res = await fetch(
    `http://127.0.0.1:8000/availability/events${page ? `?page=${page}` : ""}`,
    {
      next: { tags: ["availability_events"] },
      cache: "no-store",
    }
  );

  if (!res.ok) {
    throw new Error("Failed to fetch data");
  }

  return (await res.json()) as AvailabilityEvent[] | null;
}

export async function getAllAvailabilityEvents(): Promise<AvailabilityEvent[]> {
  let allEvents: AvailabilityEvent[] = [];
  let page = 1;
  let hasMore = true;

  while (hasMore) {
    const events = await getAvailabilityEvents(page);
    if (events) {
      allEvents = allEvents.concat(events);
      hasMore = events.length === 10;
    } else {
      hasMore = false;
    }
    page++;
  }

  return allEvents;
}
