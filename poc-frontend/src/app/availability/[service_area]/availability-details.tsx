import RenderLineChart from "@/components/time-series-chart";
import { Availability } from "@/types/availability";

async function getData(serviceArea: string, timestamp?: string) {
  const res = await fetch(
    `http://127.0.0.1:8000/availability/${serviceArea}${
      timestamp ? `?timestamp=${timestamp}` : ""
    }`,
    {
      next: { tags: ["availability"] },
      cache: "no-store",
    }
  );

  if (!res.ok) {
    throw new Error("Failed to fetch data");
  }

  return (await res.json()) as Availability;
}

export default async function AvailabilityList({
  serviceArea,
  timestamp,
}: {
  serviceArea: string;
  timestamp?: string;
}) {
  const data = await getData(serviceArea, timestamp);
  return (
    <div>
      <h2>Availability</h2>
      {data?.availability ? (
        <div style={{ display: "flex", flexDirection: "row" }}>
          <div>
            <RenderLineChart availabilityData={data.availability} />
          </div>
          <div>
            <pre>{JSON.stringify(data.availability, null, 2)}</pre>
          </div>
        </div>
      ) : (
        <div>No Availability</div>
      )}
    </div>
  );
}
