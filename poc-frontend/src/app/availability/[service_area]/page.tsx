import { Page } from "@/types/page";
import AvailabilityForm from "./availability-form";
import AvailabilityList from "./availability-details";
import AvailabilityEvents from "./availability-events";

export default async function Availability(page: Page) {
  return (
    <>
      <AvailabilityForm serviceArea={page.params.service_area as string} />
      <div
        style={{
          paddingTop: "1rem",
          display: "flex",
          flexDirection: "row",
          justifyContent: "space-between",
        }}
      >
        <AvailabilityList
          serviceArea={page.params.service_area as string}
          timestamp={page.searchParams.timestamp as string}
        />
        <AvailabilityEvents serviceArea={page.params.service_area as string} />
      </div>
    </>
  );
}
