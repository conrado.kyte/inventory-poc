import { formatDate } from "@/utils/datetime";
import Link from "next/link";
import { getAllAvailabilityEvents } from "./actions";
import { AvailabilityEvent } from "@/types/availability";

export default async function AvailabilityEvents({
  serviceArea,
}: {
  serviceArea: string;
}) {
  const data = await getAllAvailabilityEvents();
  return (
    <div>
      <h2>Events</h2>
      {Array.isArray(data) ? (
        data.map((event: AvailabilityEvent) => (
          <Link
            key={event.state.timestamp._data_}
            href={`/availability/${serviceArea}?timestamp=${Buffer.from(
              event.state.timestamp._data_
            ).toString("base64")}`}
          >
            <div
              key={event.state.timestamp._data_}
              style={{ display: "flex", flexDirection: "column" }}
            >
              <span>
                {event.topic} @ {formatDate(event.state.timestamp._data_)}
              </span>
              {event.state?.from_timestamp?._data_ && (
                <p style={{ fontSize: "0.8em" }}>
                  From:&nbsp;
                  {formatDate(event.state?.from_timestamp?._data_)}
                </p>
              )}
              {event.state?.to_timestamp?._data_ && (
                <p style={{ fontSize: "0.8em" }}>
                  To:&nbsp;
                  {formatDate(event.state?.to_timestamp?._data_)}
                </p>
              )}
            </div>
          </Link>
        ))
      ) : (
        <>
          <div>No data</div>
        </>
      )}
    </div>
  );
}
