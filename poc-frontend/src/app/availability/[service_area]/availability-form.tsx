import { revalidateTag } from "next/cache";

export default async function AvailabilityForm({
  serviceArea,
}: {
  serviceArea: string;
}) {
  async function handleSubmit(formData: FormData) {
    "use server";

    const start_date = formData.get("start_date");
    const end_date = formData.get("end_date");

    if (!start_date) {
      throw new Error("Name is required");
    }

    let encodedStartDate =
      typeof start_date === "string"
        ? Buffer.from(start_date).toString("base64")
        : "";

    let encodedEndDate =
      typeof end_date === "string"
        ? Buffer.from(end_date).toString("base64")
        : undefined;

    const resp = await fetch(
      `http://127.0.0.1:8000/availability/${serviceArea}`,
      {
        method: "POST",
        headers: new Headers({
          "content-type": "application/json",
        }),
        body: JSON.stringify({
          start_date: encodedStartDate,
          ...(encodedEndDate && { end_date: encodedEndDate }),
        }),
        cache: "no-store",
      }
    );

    revalidateTag("availability");
    return await resp.json();
  }

  return (
    <div>
      <h1>Increment Service Area</h1>
      <form action={handleSubmit} autoComplete="off">
        <div
          style={{
            paddingTop: "1rem",
            display: "flex",
            flexDirection: "row",
            gap: "20px",
          }}
        >
          <div>
            Start date{" "}
            <input type="datetime-local" name="start_date" required />
          </div>
          <div>
            End date <input type="datetime-local" name="end_date" />
          </div>
          <button type="submit">Create</button>
        </div>
      </form>
    </div>
  );
}
