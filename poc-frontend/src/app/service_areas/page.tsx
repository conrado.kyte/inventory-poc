import { Page } from "@/types/page";
import ServiceAreaEvents from "./service-areas-events";
import ServiceAreaForm from "./service-areas-form";
import ServiceAreaList from "./service-areas-list";

export default async function ServiceArea(page: Page) {
  return (
    <>
      <ServiceAreaForm />
      <div
        style={{
          paddingTop: "1rem",
          display: "flex",
          flexDirection: "row",
          justifyContent: "space-between",
        }}
      >
        <ServiceAreaList timestamp={page.searchParams.timestamp as string} />
        <ServiceAreaEvents />
      </div>
    </>
  );
}
