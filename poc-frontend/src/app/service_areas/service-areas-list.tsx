import Link from "next/link";

async function getData(timestamp?: string) {
  const res = await fetch(
    `http://127.0.0.1:8000/service_areas${
      timestamp ? `?timestamp=${timestamp}` : ""
    }`,
    {
      next: { tags: ["service_areas"] },
      cache: "no-store",
    }
  );

  if (!res.ok) {
    throw new Error("Failed to fetch data");
  }

  return await res.json();
}

export default async function ServiceAreaList({
  timestamp,
}: {
  timestamp?: string;
}) {
  const data = await getData(timestamp);
  return (
    <div>
      <h2>Service area list</h2>
      <ul>
        {Array.isArray(data?.service_areas) && data.service_areas.length ? (
          data.service_areas.map((service_area: any) => (
            <li key={service_area}>
              <Link href={`/availability/${service_area}`}>{service_area}</Link>
            </li>
          ))
        ) : (
          <li>No service areas</li>
        )}
      </ul>
    </div>
  );
}
