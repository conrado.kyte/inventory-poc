import { ServiceAreasEvent } from "@/types/service_areas";

export async function getServiceAreaEvents(page: number) {
  const res = await fetch(
    `http://127.0.0.1:8000/service_areas/events${page ? `?page=${page}` : ""}`,
    {
      next: {
        tags: ["service_areas_events"],
      },
      cache: "no-store",
    }
  );

  if (!res.ok) {
    throw new Error("Failed to fetch data");
  }

  return (await res.json()) as ServiceAreasEvent[] | null;
}

export async function getAllServiceAreaEvents(): Promise<ServiceAreasEvent[]> {
  let allEvents: ServiceAreasEvent[] = [];
  let page = 1;
  let hasMore = true;

  while (hasMore) {
    const events = await getServiceAreaEvents(page);
    if (events) {
      allEvents = allEvents.concat(events);
      hasMore = events.length === 10;
    } else {
      hasMore = false;
    }
    page++;
  }

  return allEvents;
}
