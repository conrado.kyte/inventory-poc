import { ServiceAreasEvent } from "@/types/service_areas";
import { formatDate } from "@/utils/datetime";
import Link from "next/link";
import { getAllServiceAreaEvents } from "./actions";

export default async function ServiceAreasEvents() {
  const data = await getAllServiceAreaEvents();

  return (
    <div>
      <h2>Service Area Events</h2>
      {Array.isArray(data) ? (
        data.map((event: ServiceAreasEvent, i) => (
          <div key={event.state.timestamp._data_}>
            <Link
              href={`/service_areas?timestamp=${Buffer.from(
                event.state.timestamp._data_
              ).toString("base64")}`}
            >
              {event.topic} @ {formatDate(event.state.timestamp._data_)}
            </Link>
          </div>
        ))
      ) : (
        <>
          <div>No data</div>
        </>
      )}
    </div>
  );
}
