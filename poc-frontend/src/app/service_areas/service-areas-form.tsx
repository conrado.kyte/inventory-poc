import { revalidateTag } from "next/cache";

export default async function ServiceAreaForm() {
  async function handleSubmit(formData: FormData) {
    "use server";

    const name = formData.get("name");

    if (!name) {
      throw new Error("Name is required");
    }

    const resp = await fetch("http://127.0.0.1:8000/service_areas", {
      method: "POST",
      headers: new Headers({
        "content-type": "application/json",
      }),
      body: JSON.stringify({ name }),
      cache: "no-store",
    });

    revalidateTag("serviceAreaEvents");
    return await resp.json();
  }

  return (
    <div>
      <h1>Create Service Area</h1>
      <form action={handleSubmit} autoComplete="off">
        <div
          style={{
            paddingTop: "1rem",
            display: "flex",
            flexDirection: "row",
            gap: "20px",
          }}
        >
          <input type="text" name="name" />
          <button type="submit">Create</button>
        </div>
      </form>
    </div>
  );
}
