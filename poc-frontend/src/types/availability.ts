export type Availability = {
  service_area: string;
  availability: {
    [key: string]: {
      availability: number;
    };
  };
};

export type AvailabilityEvent = {
  topic: string;
  state: {
    timestamp: {
      __type__: string;
      _data_: string;
    };
  };
};
