export type ServiceAreasEvent = {
  topic: string;
  state: {
    timestamp: {
      __type__: string;
      _data_: string;
    };
  };
};
