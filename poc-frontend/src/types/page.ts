export type Page = {
  params: { [key: string]: string };
  searchParams: { [key: string]: string | string[] | undefined };
};

export type PaginatedResult<T> = {
  data: T;
  pages: number;
};
