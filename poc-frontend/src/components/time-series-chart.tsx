"use client";

import { Availability } from "@/types/availability";
import { formatDate } from "@/utils/datetime";
import { useMemo, useState } from "react";
import {
  LineChart,
  Line,
  CartesianGrid,
  XAxis,
  YAxis,
  Tooltip,
  Brush,
} from "recharts";

type CustomTooltipProps = {
  active?: boolean;
  payload?: any[];
  label?: string;
};

const CustomTooltip = ({ active, payload, label }: CustomTooltipProps) => {
  if (active && payload && payload.length) {
    return (
      <div
        className="custom-tooltip"
        style={{
          backgroundColor: "white",
          padding: "10px",
          border: "1px solid #ccc",
        }}
      >
        <p className="label">{`Date: ${formatDate(label)}`}</p>
        <p className="intro">{`Availability: ${payload[0].value}`}</p>
      </div>
    );
  }

  return null;
};

const CustomizedAxisTick = (props: any) => {
  const { x, y, stroke, payload } = props;

  return (
    <g transform={`translate(${x},${y})`}>
      <text
        x={0}
        y={0}
        dy={16}
        textAnchor="end"
        fill="#666"
        transform="rotate(-35)"
      >
        {formatDate(payload.value)}
      </text>
    </g>
  );
};

export default function RenderLineChart({
  availabilityData,
}: {
  availabilityData: Availability;
}) {
  const [state, setState] = useState(true)
  const data = useMemo(() => {
    const result = [];

    for (let date in availabilityData) {
      result.push({
        date: date,
        availability: availabilityData[date].availability,
      });
    }

    return result;
  }, [availabilityData]);

  return (
    <LineChart
      width={600}
      height={300}
      data={data}
      margin={{ top: 5, right: 30, left: 20, bottom: 70 }}
    >
      <CartesianGrid strokeDasharray="3 3" />
      <XAxis
        dataKey="date"
        height={60}
        interval={0}
        tick={<CustomizedAxisTick />}
      />
      <YAxis />
      <Tooltip content={<CustomTooltip />} />
      <Line
        type="monotone"
        dataKey="availability"
        stroke="#098A22"
        activeDot={{ r: 8 }}
      />
      <Brush y={260} />
    </LineChart>
  );
}
